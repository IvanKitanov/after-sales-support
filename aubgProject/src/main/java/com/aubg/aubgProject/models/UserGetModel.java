package com.aubg.aubgProject.models;

import com.aubg.aubgProject.entities.Role;

public class UserGetModel {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private String phone;

   public Integer getId(){

        return id;
    }
    public void setId(Integer id) {

        this.id = id;
    }
    public String getFirstName() {

        return firstName;
    }
    public void setFirstName(String firstName){

        this.firstName = firstName;
    }
    public String getLastName() {

        return lastName;
    }
    public void setLastName(String lastName){

        this.lastName = lastName;
    }
    public String getEmail(){
        return email;
    }
    public void setEmail(String email){

        this.email = email;
    }

    public String getRole() {
        return role.getName();
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
