package com.aubg.aubgProject.models;


import com.aubg.aubgProject.entities.Role;

public class UserPostModel {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private  String phone;
    private Role role;

    public String getFirstName(){return firstName;}
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    public String getLastName()
    {
        return lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    public String getEmail(){return email;}
    public void setEmail(String email){
        this.email = email;
    }
    public String getPassword(){return password;}
    public void setPassword(String password)
    {
        this.password = password;
    }
    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }
    public String getPhone() {return phone;}
    public void setPhone(String phone) {this.phone = phone;}
}
