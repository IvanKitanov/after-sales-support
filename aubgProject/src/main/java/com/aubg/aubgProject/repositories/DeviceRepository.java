package com.aubg.aubgProject.repositories;

import com.aubg.aubgProject.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<Device, Integer>{

}
