package com.aubg.aubgProject.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="users", schema = "aubgprojetct")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
    @ManyToOne
    @JoinColumn(name="roles_id")
    private  Role role;
    @OneToMany( mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Device> devices= new ArrayList<>();

    public User(){
        super();

    }
    public User(String firstName, String lastName, String email, String phone, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;

    }
    public String getFirstName(){

        return firstName;
    }
    public void setFirstName(String firstName){

        this.firstName = firstName;
    }
    public String getLastName(){

        return lastName;
    }
    public void setLastName(String lastName){

        this.lastName = lastName;
    }
    public String getEmail(){

        return email;
    }
    public void setEmail(String email){

        this.email = email;
    }
    public String getPassword(){

        return password;
    }
    public void setPassword(String password){

        this.password = password;
    }

    public Integer getId(){

        return id;
    }
    public void setId(Integer id){

        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(password, user.password) &&
                Objects.equals(role, user.role);
    }
}
