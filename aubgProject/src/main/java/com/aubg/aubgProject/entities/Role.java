package com.aubg.aubgProject.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

    @Entity
    @Table(name ="roles",schema = "aubgproject")
    public class Role {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;
        private String name;
        @OneToMany( mappedBy = "role",cascade = CascadeType.ALL,orphanRemoval = true)
        private List<User> users= new ArrayList<>();

        public Role(){
            super();
        }
        public Role(Integer id, String name){
            this.name=name;
            this.id=id;

        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<User> getUsers() {
            return users;
        }

        public void setUsers(List<User> users) {
            this.users = users;
        }

        public void addUser(User user){
            users.add(user);
        }

        public void removeUser(User user){
            users.remove(user);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Role)) return false;
            Role role = (Role) o;
            return Objects.equals(getId(), role.getId()) &&
                    Objects.equals(getName(), role.getName());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getId(), getName());
        }
}
