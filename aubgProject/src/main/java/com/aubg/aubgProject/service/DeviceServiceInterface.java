package com.aubg.aubgProject.service;

import com.aubg.aubgProject.entities.Device;

import java.util.List;
import java.util.Optional;

public interface DeviceServiceInterface {
    Optional<Device> get(Integer id);
    List<Device> get();
    Optional<Device> create(Device device);
    Optional<Device> update(Device device);
    void delete(Integer id);
}
