package com.aubg.aubgProject.service;

import com.aubg.aubgProject.entities.Role;
import com.aubg.aubgProject.entities.User;
import com.aubg.aubgProject.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserServiceInterface{
    private UserRepository userRepository;
    private RoleService service;
    private ModelMapper modelMapper;

    @Autowired
    public UserService(UserRepository userRepository, RoleService service, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<User> get(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> get() {

        return userRepository.findAll();
    }

    @Override
    public Optional<User> create(User user) {
        Optional<Role> optionalRole=service.getByName("ROLE_CUSTOMER");
        if(optionalRole.isPresent()){
            Role role= optionalRole.get();
            user.setRole(role);
            return Optional.of(userRepository.save(user));
        }
        return null;
    }
    @Override
    public Optional<User> promote(User user, String roleName){
        Optional<Role> optionalRole=service.getByName(roleName);
        if(optionalRole.isPresent()){
            Role role= optionalRole.get();
            user.setRole(role);
            return Optional.of(userRepository.save(user));
        }
        return null;
    }

    @Override
    public Optional<User> update(User user) {

        return Optional.of(userRepository.save(user));
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }


}
