package com.aubg.aubgProject.service;

import com.aubg.aubgProject.entities.Device;
import com.aubg.aubgProject.entities.User;
import com.aubg.aubgProject.repositories.DeviceRepository;
import com.aubg.aubgProject.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceService  implements DeviceServiceInterface{
    private DeviceRepository deviceRepository;
    private UserService userService;
    private ModelMapper modelMapper;
    @Autowired
    public DeviceService(DeviceRepository deviceRepository,UserService userService,ModelMapper modelMapper){
        this.deviceRepository=deviceRepository;
        this.userService=userService;
        this.modelMapper=modelMapper;
    }

    @Override
    public Optional<Device> get(Integer id) {
        return deviceRepository.findById(id);
    }

    @Override
    public List<Device> get() {
        return deviceRepository.findAll();
    }

    @Override
    public Optional<Device> create(Device device) {

        return Optional.of(deviceRepository.save(device));
    }

    @Override
    public Optional<Device> update(Device device) {

        return Optional.of(deviceRepository.save(device));
    }

    @Override
    public void delete(Integer id) {
        deviceRepository.deleteById(id);
    }

}
