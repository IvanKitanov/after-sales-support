package com.aubg.aubgProject.service;

import com.aubg.aubgProject.entities.User;
import com.aubg.aubgProject.repositories.UserRepository;

import java.util.List;
import java.util.Optional;

public interface UserServiceInterface  {
    Optional<User> get(Integer id);
    List<User> get();
    Optional<User> create(User user);
    Optional<User> promote(User user, String roleName);
    Optional<User> update(User user);
    void delete(Integer id);
    Boolean existsByEmail(String email);
}
