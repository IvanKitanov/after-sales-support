package com.aubg.aubgProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AubgProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AubgProjectApplication.class, args);
	}

}
