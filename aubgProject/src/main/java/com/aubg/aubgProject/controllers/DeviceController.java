package com.aubg.aubgProject.controllers;

import com.aubg.aubgProject.entities.Device;
import com.aubg.aubgProject.entities.User;
import com.aubg.aubgProject.models.DeviceGetModel;
import com.aubg.aubgProject.models.DevicePostModel;
import com.aubg.aubgProject.models.UserGetModel;
import com.aubg.aubgProject.payload.ApiResponse;
import com.aubg.aubgProject.service.DeviceService;
import com.aubg.aubgProject.service.UserService;
import com.aubg.aubgProject.service.UserServiceInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/device")
public class DeviceController {
    private UserService userService;
    private ModelMapper modelMapper;
    private DeviceService deviceService;

    @Autowired
    public DeviceController(UserService userService, ModelMapper modelMapper, DeviceService deviceService) {
        this.deviceService = deviceService;
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @GetMapping
    public HttpEntity get() {
        List<Device> devices = deviceService.get();
        List<DeviceGetModel> deviceGetModels = new ArrayList<>();
        for (Device device : devices) {
            deviceGetModels.add(modelMapper.map(device, DeviceGetModel.class));
        }
        return ResponseEntity.ok(deviceGetModels);
    }

    @GetMapping(value = "/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Device> optionalDevice = deviceService.get(id);
        if (optionalDevice.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalDevice.get(), DeviceGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @PostMapping
    public HttpEntity create(@RequestBody DevicePostModel devicePostModel) {
        Device mapped = modelMapper.map(devicePostModel, Device.class);
        if (devicePostModel.getUserId() == null)
            return new ResponseEntity(new ApiResponse(false, "Please specify User ID in order to complete with device creation"),
                    HttpStatus.BAD_REQUEST);
        Optional<User> optionalUser = userService.get(devicePostModel.getUserId());
        if (optionalUser.isPresent()) {
            mapped.setUser(optionalUser.get());
        } else return new ResponseEntity(new ApiResponse(false, "User with this id cannot be found!"),
                HttpStatus.BAD_REQUEST);
        Optional<Device> optionalDevice = deviceService.update(mapped);
        if (optionalDevice.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalDevice.get(), DeviceGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @PutMapping(value = "/{id}")
    public HttpEntity update(@PathVariable("id") Integer id, @RequestBody DevicePostModel devicePostModel) {
        Optional<Device> optionalDevice = deviceService.get(id);
        if (!optionalDevice.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        Device mapped = modelMapper.map(devicePostModel, Device.class);
        mapped.setId(id);
        if (devicePostModel.getUserId() == null)
            return new ResponseEntity(new ApiResponse(false, "Please specify User ID in order to complete with device creation"),
                    HttpStatus.BAD_REQUEST);
        Optional<User> optionalUser = userService.get(devicePostModel.getUserId());
        if (optionalUser.isPresent()) {
            mapped.setUser(optionalUser.get());
        } else return new ResponseEntity(new ApiResponse(false, "User with this id cannot be found!"),
                HttpStatus.BAD_REQUEST);
        optionalDevice=deviceService.create(mapped);
        if (optionalDevice.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalDevice.get(), DeviceGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();

    }

    @DeleteMapping(value = "/{id}")
    //@PreAuthorize("hasRole('ROLE_ADMIN'), hasRole('ROLE_ENGINEER')")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Device> optionalDevice = deviceService.get(id);
        if (!optionalDevice.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        deviceService.delete(id);
        return ResponseEntity.noContent()
                .build();
    }
}
