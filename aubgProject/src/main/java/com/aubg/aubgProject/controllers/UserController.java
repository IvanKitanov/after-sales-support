package com.aubg.aubgProject.controllers;

import com.aubg.aubgProject.entities.Role;
import com.aubg.aubgProject.entities.User;
import com.aubg.aubgProject.models.UserGetModel;
import com.aubg.aubgProject.models.UserPostModel;
import com.aubg.aubgProject.security.UserPrincipal;
import com.aubg.aubgProject.service.RoleService;
import com.aubg.aubgProject.service.UserServiceInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {
    private UserServiceInterface service;
    private ModelMapper modelMapper;
    private RoleService roleService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserServiceInterface service, ModelMapper modelMapper, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping(value = "/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<User> optionalUser = service.get(id);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @GetMapping
    public HttpEntity get() {
        List<User> users = service.get();
        List<UserGetModel> userGetModels = new ArrayList<>();
        for (User user : users) {
            userGetModels.add(modelMapper.map(user, UserGetModel.class));
        }
        return ResponseEntity.ok(userGetModels);
    }

    @GetMapping(value = "/currentUser")
    public HttpEntity getCurrentUser() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserGetModel userGetModel = modelMapper.map(userPrincipal, UserGetModel.class);
        return ResponseEntity.ok(userGetModel);
    }

    @PostMapping
    public HttpEntity create(@RequestBody UserPostModel userPostModel) {
        User mapped = modelMapper.map(userPostModel, User.class);
        mapped.setPassword(passwordEncoder.encode(mapped.getPassword()));
        Optional<User> optionalUser = service.create(mapped);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @PutMapping(value = "/{id}/changeRole/{number}")
     public HttpEntity changeRole(@PathVariable("id") Integer id, @PathVariable("number") int number, @RequestBody UserPostModel userPostModel) {
        Optional<User> optionalUser = service.get(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        User mapped = modelMapper.map(userPostModel, User.class);
        mapped.setId(id);
        mapped.setPassword(passwordEncoder.encode(mapped.getPassword()));
        String roleName=null;
        if(number==1) roleName="ROLE_CUSTOMER";
        if(number==2) roleName="ROLE_ADMIN";
        if(number==3) roleName="ROLE_ENGINEER";
        if(number==4) roleName="ROLE_MANAGER";
        if(number==5) roleName="ROLE_RECEPTIONIST";
        if(number < 1 || number > 5) return ResponseEntity.badRequest().body("Please specify a role from 1 to 5");
        if (number >= 1 || number <= 5){
            Optional<Role> optionalRole = roleService.getByName(roleName);
            if (optionalRole.isPresent()) {
                service.promote(mapped, roleName);
            }
        }
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<User> optionalUser = service.get(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        service.delete(id);
        return ResponseEntity.noContent()
                .build();
    }

}